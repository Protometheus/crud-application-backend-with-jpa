--------------------------------------------------------
--  DDL for Table USER_INFO_TB
--------------------------------------------------------

CREATE TABLE USER_INFO_TB
(
    USERID VARCHAR2(30), 
	USERPW VARCHAR2(15), 
	EMAIL VARCHAR2(60) UNIQUE, 
	TEL VARCHAR2(20), 
	ADDRESS VARCHAR2(50),
    PRIMARY KEY(USERID)
);

Insert into USER_INFO_TB (USERID,USERPW,EMAIL,TEL,ADDRESS) values ('123','123','123','12322','123');
Insert into USER_INFO_TB (USERID,USERPW,EMAIL,TEL,ADDRESS) values ('11','11','11','11','11');
Insert into USER_INFO_TB (USERID,USERPW,EMAIL,TEL,ADDRESS) values ('1234','1234','1234','1234','1234');

