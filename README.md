<div style="text-align:center"><h3>CRUD Application - Backend</h3></div>

------

###   INDEX

1. ##### Introduce

2. ##### Stack

------

1. ##### Introduce

   React + Spring Framework를 사용하는 웹 개발 관련 Study를 위한 보일러 플레이트입니다.

   간단한 CRUD 기능이 구현되어있습니다.

   

2. ##### Stack

   Java

   Spring Boot 2.2.6

   DBMS -  Oracle 11.2g

   JPA

   lombok