package com.genergy.edu.repository;

import org.springframework.data.repository.CrudRepository;

import com.genergy.edu.entity.UserInfo;

public interface UserInfoRepository extends CrudRepository<UserInfo, String> {	

}
