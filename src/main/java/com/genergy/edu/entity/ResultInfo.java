package com.genergy.edu.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultInfo {

	private String title;
	private String desc;
	
}
