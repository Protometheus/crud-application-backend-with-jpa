package com.genergy.edu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "USER_INFO_TB")
public class UserInfo {

	@Id
	@Column(name = "USERID", length = 30)
	private String userId;
	@Column(name = "USERPW", length = 15)
	private String userPw;
	@Column(name = "EMAIL", length = 60, unique = true)
	private String email;
	@Column(name = "TEL", length = 20)
	private String tel;
	@Column(name = "ADDRESS", length = 50)
	private String address;
	
}
