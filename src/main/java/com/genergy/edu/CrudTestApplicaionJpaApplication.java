package com.genergy.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudTestApplicaionJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudTestApplicaionJpaApplication.class, args);
	}

}
