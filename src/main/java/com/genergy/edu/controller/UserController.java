package com.genergy.edu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.genergy.edu.entity.ResultInfo;
import com.genergy.edu.entity.UserInfo;
import com.genergy.edu.service.UserInfoService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserInfoService userInfoService;
	
	@PostMapping
	public ResultInfo create(@RequestBody UserInfo userInfo) {
		return ResultInfo.builder()
							.title("계정 생성")
							.desc(userInfoService.add(userInfo))
							.build();
	}
	
	// 단순 테스트 용도
	@GetMapping
	public List<UserInfo> findAll() {
		return userInfoService.retrieveAll();
	}
	
	@GetMapping("/{userId}")
	public UserInfo findById(@PathVariable String userId) {
		return userInfoService.retrieve(userId);
	}
	
	@PutMapping
	public ResultInfo modify(@RequestBody UserInfo userInfo) {
		return ResultInfo.builder()
								.title("계정 수정")
								.desc(userInfoService.modify(userInfo))
								.build();
	}
	
	@DeleteMapping("/{userId}")
	public ResultInfo remove(@PathVariable String userId) {
		return ResultInfo.builder()
								.title("계정 삭제")
								.desc(userInfoService.remove(userId))
								.build();
	}
	
}
