package com.genergy.edu.service.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genergy.edu.entity.UserInfo;
import com.genergy.edu.repository.UserInfoRepository;
import com.genergy.edu.service.UserInfoService;

@Service
public class UserInfoServiceLogic implements UserInfoService {
	
	@Autowired
	private UserInfoRepository userInfoRepository;
	
	@Override
	public String add(UserInfo userInfo) {
		return (userInfoRepository.save(userInfo) != null) ? "success" : "fail";
	}

	@Override
	public String modify(UserInfo userInfo) {
		return (userInfoRepository.save(userInfo) != null) ? "success" : "fail";
	}

	@Override
	public String remove(String userId) {
		Optional<UserInfo> targetUser = userInfoRepository.findById(userId);
		
		if (targetUser.isPresent()) {
			userInfoRepository.delete(targetUser.get());
			
			return "success";
		}
		
		return "fail";
	}

	@Override
	public UserInfo retrieve(String userId) {
		Optional<UserInfo> targetUser = userInfoRepository.findById(userId);
		
		return (targetUser.isPresent()) ? targetUser.get() : null;
	}

	@Override
	public List<UserInfo> retrieveAll() {
		List<UserInfo> userList = new ArrayList<UserInfo>();
		
		userInfoRepository.findAll().forEach(userList::add);
		
		return userList;
	}
	
}
